#!/bin/bash
#run from project folder (outside of library folder)
#updates documentation in the ./htmlDoc folder
for filename in ./monkeyDatabase/man/*.Rd; do
    R CMD Rdconv -t html "$filename" > "htmlDoc/$(basename "$filename" .Rd).html"
done
