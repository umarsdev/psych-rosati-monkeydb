#Track Monkey data for the Cognitive Evolution Group

This will consist of two main components:
1. a MongoDB database to store all data
2. a web interface in R Shiny to access the database


##The R functions for general use are found in the 'importStudy.R' file

Work with this file by sourcing it appropriately: `source('importStudy.R')`
functions available are:

`censusImport(xlsxFile, site, effectiveDate, alwaysAcceptChanges, isNotchList)`

`ratingDF <- setCooperativeRating()`
    This function searches all studies in the database and updates the census table with the appropriate values. It also returns a summary

`manualCensusChange(fullName, type, newValue)`
    Given a fullName, type (category to update), and newValue, this function will set the Census database table information for an animal. If newValue is NULL then the category will be removed for that individual.

`importStudy(xlsxFile, tabList, isAnalysis, site)`
    Import a study file. The tabList should be `tabList <- c('Key', 'All', 'Completed', 'Final subjects')` or `analysisTabs <- c('Key', 'Data')` when importing an analysis file. `isAnalysis` should be `TRUE` if it is an analysis file, and the data file must be imported first. This function create both the appropriate tables to hold the study, and creates an entry in the studies table which holds all of the meta data.

R shiny server is started with `source ShinyCensus.R` and then `runApp(app, host = "0.0.0.0")`

Note that some of the monkeys have names that are recognized as scientific notation. Avoid csv import errors in excel with:
    1. Open a new workbook
    2. Select the Data menu, and click the "From Text" button
	3. On the last section of the import tool make sure to select the name column as text. All others can probably be left as general

The final version will be hosted on a yottabyte (YRC?) instance named rosatidb.psych.lsa.umich.edu
The R interface could be run locally, or accessed via on the computer hosting the database

YRC was chosen because of the cost (free) for this project.

debug using local mongoDB:
run 'mongod' to start the server
run 'mongo' to get a shell to access the database and make simple querries/edits





###Server Setup:
Assuming a starting point of a Linux server with R installed

 1. generate, copy SSH keys to BitBucket
 2. Install mongodb with instructions from Digital Ocean: https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-16-04
 3. install libssl-dev `sudo apt install libssl-dev` and `sudo apt install libsasl2-dev` (needed for the R openssl package)
 4. Install R packages by changing to shiny user with:
 su - shiny
 note: shiny user password in lastPass
 `
 install.packages('mongolite')
 install.packages('openssl')
 install.packages('shinythemes')
 install.packages('DT')
 install.packages('openxlsx')
 install.packages('dplyr')
 `
source('ShinyCensus.R')
 runApp(app, host = "0.0.0.0", port = 3926)
 5. Get code via git. Upload a set of ssh keys if it is password protected.
 6. configure to allow network traffic to mongodb with networkSetup.sh script

 7. install shiny server with instructions from: https://docs.rstudio.com/shiny-server/#ubuntu-14.04
 8. change shiny server setup with /etc/shiny-server/shiny-server.conf
    shiny logs go to /var/log/shiny-server.log
    restart shiny server with sudo systemctl restart shiny-server

 9.  

if network issues check kernel logs at: /var/log/kern.log
check running services with:
service --status-all
start mongodb at start with:
systemctl enable mongod.service

###Documentation
update documentation by going to the library folder (monkeyDatabase)
https://rawgit.com/rstudio/cheatsheets/master/package-development.pdf
library(devtools)
library(roxygen2)
document()
update html documentation with:
./generateHTMLDocumentation

###Copy database over with
mongodump --db monkeyTest
tar -cxf monkeyDump.tar.gz ./dump/
scp monkeyDump.tar.gz lsa@rosatidb.psych.lsa.umich.edu:monkey.tar.gz
on remote server:
    tar zxf monkey.tar.gz
    mongorestore ./dump


##7-20-18 Meeting:

Online way to upload census file
	- highlight any changes (possible typos)
	- setup as: trial upload that just shows issues, and real upload that is active.
Cooperative monkey list from website
	- should have sex and current group
	- How to compare this to notch list
Get notch list and add that data to census

Study data -> notebook

Get info about exclusion ratio
	- how many monkeys tested
	- how many excluded for each reason
	- probably added as another column in the excel spreadsheet

For analysis
	- R Script that takes a list of [study [list of columns of interest]] and returns the monkeys that overlap between all of the studies with their results. Bool for only show monkeys in ALL studies

##7-24-18 meeting:
Automate filling out notebook file. Get name and fill in the rest. In the future
all ID update to "Subject"
get all column names from a Notebook file
a way to handle lab internal notes
GROUP is birth group
ngroup is new group!!!!!

QUESTION:
how to limit access. Need to just limit to Read/Write, or need to do it study by study.

studyTable change to "notebookTable" everywhere it shows in the code

ISSUES ADDING NEW SITE:
[ ] How to exclude infants from study ratings? Currently we check for 3 chars which won't work
7-31-18 meeting.
[ ] status column for marking present / dead
[x] way to capture infant -> adult transition
	match based on DOB && MOM. Add column for infant ID. Searches should also search for their infant ID
[ ] any time you search for studies.... if no adult name found then also search by their infant name
[x] duplicate names. Can name, location, birth year be sufficient to differentiate.
    Lab with manually remove duplicates. Probably by appending a character.
[ ] import census: when DOB changes ask if new individual, or update record, or ignore

8-2-18 final meeting including Megan
todo:
[x] Update to make ID based on name, site. Other duplicates will be handled internally
[x] Handle infant -> adult transition on census import
	-possibly allow sex switch
	-based mostly on mom/DOB
[ ] Function to manually change census
[ ] Census Import:
	- Any changes to census should be logged
	- Three modes: Interactive, Just create issue log, accept all changes (if you went through the issue log and sorted things out manually yourself)
[x] Replace participation score of 0/0 with blank
[ ] Participation should include MOM, DOB, EVERYTHING FROM CENSUS
[ ] Current group should reflect CENSUS, don't change based on notch list
[ ] R functions to get:
	- All study data for a monkey
	- Info for a given study
	- Participation info
	- ...
[ ] Import census and studies from Trenthum
[ ] Get DB, shiny running on server "rosatidb.psych.lsa.umich.edu"

* We also discussed the project status/funding. Goal is to have the current stuff done for September, and then we will put together a new programming agreement to cover the other sites/species.

9-28 Rosati and Gabrielle
[x] Get monkeys from combinations of studies. Should take a list of studies and get all of the monkeys in both, or all of the monkeys in either.
[?] Get all monkeys that were studied in a given time range. Giving a list. Is that what they want?
[-] Apes will be coming up soon
[x] Date Monkey was last seen in a census
[x] Approximate death age based on DOB, last census
    ? probably not in census. calc on demand
[x] participation by STUDY not per trial
[x] add project leader as an entry in study table
[ ] How much does census data change? Give Alex an example change list from the last few censuses
[x] Studies get an entry for the publication title. Be able to search for all studies with the same publication.
[x] infant check (could be upper or lowercase I)
[ ] don't display SEASON online, maybe don't import
[ ] Caio, Trenthum for site names
[ ] add species to census
[ ] error checking online version: don't crash
[x] function to remove study so we can re-import to edit
[ ] Check for tabs "Pilot Subjects" and if so add a note to the census metadata
[ ] On importing Pedigree: Message if mom and DAM don't match, add note about kidnapping

10-12 with Gabrielle
[ ] Study name repeats? Names could repeat. Add species as part of the key.
[x] Force cooperation to recalculate when you add/remove a study.
[ ] Error checking on column name to catch trailing spaces, alternate caps, ...
web:
[ ] search by left ear, right ear, filter by sex, current group.
    - need to sort results by current group
    - button to add missing monkeys incase they were hiding during the last census. Maybe show only ones born in the last 30 years
[x] notch list find missing columns
[x] import all census data, notch data and email lab
[x] get rid of header column (just numbers)
[n] csv export format that forces a column to be a string. Prevent excel from taking numbers into 'general' format.
[x] list of current function

Nov 2
[ ] cooperative list: search by ear, sort by gender, group
[NO] Trenthum notes: compress to a single "Notes". Keep notes separate
[x] What happened to group history ??!?!?!
[ ] don't auto accept current group of female.
[x] cooperative list show by study not trials for each type
[x] Move study participation to next to the ear notch info on cooperative list so that it is easy to see
[X] remember to get value for if it has either a "Pilot Subjects" or "Pilot" tab


Nov 9
[ ] census "find parents"
[ ] Trenthum notes: keep notes separate
[ ] Census. just "Birth Season". Not Season from census
[ ] Cooperative monkey list: just last census. Not last notch list.
[ ] Allow studies with the same name, date range across sites
[X] get the pilot data when you request a download of the study data - just downlaod the full file.
[ ] Some columns not present on Trenthum census.

Nov 30
[X] Make sure monkeys in date range can capture either Analysis, or all tested monkeys
[X] don't let people download CSV files
[NO] import a tab delimited study file.
[ ] waiting on answer: they will import just notebook. Will they need to import just analysis for any reason?
[ ]
Dec 15
[X] Plan for handling need to retrieve xls files: Will upload file with rcurl to a location accessible via http. I will then give a link to that file in the R Shiny list of studies view.

Jan 11
[ ] Clean up the census imports next:
    [X] import Trenthum census. Ignore missing columns. Keep all notes
    [ ] Options for import processing: walk through each change, or accept and log all changes for review. Append to a local censusChanges.txt file.
just finished:
    - fix up monkeysInDateRange()
    - store original xlsx files on the server
    - allow same study across sites (turned out to be an easy fix)
For next week:
[ ] example output from census imports, walk through one
[ ] R Shiny up on yattabyte

Jan 18
[X] split up routine changes and unexpected changes
routine: Infant name (NOT TWINS), male change groups
    Ok to split between two files.
[x] change auto accept to be auto or log only
[x] censusChanges each get their own file named 'censusChanges_2017-02-01.txt'
[X] recommended Excel training?
Feb 01
[ ] On study upload: compare to census in database
[ ] census and studies a folder at a time
FEB 08
[ ] compareCensus(file, ShowMakeChangeGroup??) - add an option to hide males changing groups (still show them if there was EVER a time where they were reported as female)
[ ] Update documentation (function list)
FEB 13
[ ] Hide dataFileID... from the web interface.
[ ] web interface : always everything on one page instead of pages of 20
[ ] "Time" in studies tables should be a time of day
[ ] When viewing study table in web interface show "study name" and project leader at the top of the page.
[ ] compareCensus hideMakeGroup change option not working: always hides
[ ]
