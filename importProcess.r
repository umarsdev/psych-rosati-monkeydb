#import process of few demo files:


##testing with a longer list:
setwd("~/Documents/Psych-Rosati-MonkeyDB")
source('importStudy.R')
cleanup()


censusImport("/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-census/Census-April-2016-FIXED.xlsx", 'Cayo', "2016-04-01", TRUE, FALSE)
censusImport("/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-census/Census-February-2017-FIXED.xlsx", 'Cayo', "2017-02-01", TRUE, FALSE)
censusImport("/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-census/Census-June-2017-FIXED.xlsx", 'Cayo', "2017-06-01", FALSE, FALSE)
censusImport('/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-census/Notchlist-February-2017-FIXED.xlsx', 'Cayo', '2017-02-01', FALSE, TRUE)

#test upgrading to adult:
censusImport("/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-census/Census-June-2017-FIXED.xlsx", 'Cayo', "2017-06-01", TRUE, FALSE)
censusImport("/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-census/Census-June-2017-OneToAdult.xlsx", 'Cayo', "2017-06-02", TRUE, FALSE)


tabList <- c('Key', 'All', 'Completed', 'Final subjects')
analysisTabs <- c('Key', 'Data')
#Notebook Files:
studyList <- c("/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Approach-studies/Notebook_metacognition_5.24.18-DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Approach-studies/Notebook_selectiveexploration_5.24.18_DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Gaze-studies/Notebook_rhesus_gazecontrol_5.24.18-DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Gaze-studies/Notebook_rhesus_gazemain_5.24.18-DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Notebook_bingo1_5.21.18_DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Notebook_bingo2_5.21.18_DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Notebook_bingo3_5.21.18_DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Notebook_negativeemotions_5.24.18-DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Notebook_predictionage_5.24.18_DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Notebook_solidityviolation_5.24.18_DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Notebook_stats1_6.25.18_DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Notebook_stats2_6.25.18_DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Notebook_stats3_6.25.18_DATABASE.xlsx")


#Analysis Files:
analysisList <- c("/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Approach-studies/Analysis_metacognition_5.24.18-DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Approach-studies/Analysis_selectiveexploration_5.24.18_DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Gaze-studies/Analysis_rhesus_gazecontrol_5.24.18-DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Gaze-studies/Analysis_rhesus_gazemain_5.24.18-DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Analysis_bingo1_5.24.18_DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Analysis_bingo2_5.24.18_DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Analysis_bingo3_5.24.18_DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Analysis_negativeemotions_6.26.18-DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Analysis_predictionage_5.24.18_DATBASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Analysis_solidityviolation_5.24.18_DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Analysis_stats1_6.26.18_DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Analysis_stats2_6.26.18_DATABASE.xlsx",
"/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Looking-time-studies/Analysis_stats3_6.26.18_DATABASE.xlsx")


analysisList <- c("/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-data6-22-18/Approach-studies/Analysis_metacognition_5.24.18-DATABASE.xlsx")

for(study in studyList){importStudy(study, tabList, FALSE, 'Cayo')}
for(study in analysisList){
	print(paste("starting:", study))
	importStudy(study, analysisTabs, TRUE, 'Cayo')
	print(paste("handled: ", study))
}

#test getting monkey's study data:





##fix census after cooperative rating bug:
db.census.remove({})
censusImport("/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-census/Census-June-2017-FIXED.xlsx", 'Cayo', "06/01/17", TRUE, FALSE)
censusImport('/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-census/Notchlist-February-2017-FIXED.xlsx', 'Cayo', '07/23/18', TRUE, TRUE)
ratings <- setCooperativeRating()


########cleanup code:
#read list of studies from "studies" table

studyListDB <- mongo(url = "mongodb://127.0.0.1", db= "monkeyTest", collection = "studies")
studyNameList <- studyListDB$find("{}", fields = "{\"studyTable\":1, \"analysisTable\":1, \"_id\":0}")

for(rowNum in 1:nrow(studyNameList)){
	print("dropping another set:")
	studyName <- studyNameList[rowNum,1]
	analysisName <- studyNameList[rowNum,2]
	print(studyName)
	studyDB <- mongo(url = "mongodb://127.0.0.1", db= "monkeyTest", collection = studyName)
	studyDB$drop()
	analysisDB <- mongo(url = "mongodb://127.0.0.1", db= "monkeyTest", collection = analysisName)
	analysisDB$drop()
}
studyListDB$drop()


####adding a notch list:
rm(list = ls())
source('importStudy.R')
censusImport('/Users/crstock/Documents/Psych-Rosati-MonkeyDB/Rhesus-census/Notchlist-February-2017-FIXED.xlsx', '07/23/18', 0, 1)

source('ShinyCensus.R')



#R Notes

#typing starts to get slow? Try Ctrl+Cmd+L or rm(list = ls())
#debug by setting: options(error=recover)
